package getRequest;

import static io.restassured.RestAssured.*;
import io.restassured.response.Response;
import org.testng.annotations.Test;


import static org.testng.Assert.assertEquals;

public class GetValidateResponse {

    @Test
    public void testReponseCode(){

        Response resp = get("https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22");

        int code = resp.getStatusCode();

        System.out.println("Status code is: " + code);

        assertEquals(code, 200);
    }

    @Test
    public void testBody(){

        Response resp = get("https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22");

        String data = resp.asString();   // turn response from JSON to String
        System.out.println("Data is: " + data);

        String contentType = resp.getContentType();
        System.out.println("Response time is: " + resp.getTime() + " \nContent Type is: " + contentType);

    }
}
